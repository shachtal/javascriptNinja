originalArray=[
	{name:"tal",gender:"male"},
	{name:"rogerRabit",gender:"male"},
	{gender:"hermaphrodite"},
	{name:"kipiBenKipod"},
	{name:"boyGeorge",gender:"queer"},
	{name:"golda meir",gender:"female"}
]
var hasGenderAndName = x=>x.gender&&x.name
var addNamePrefix=x =>	x.gender=="male"	?	Object.defineProperty(x,"name",{value: "Mr. "+x.name}): 			  	
						x.gender=="female"	?	Object.defineProperty(x,"name",{value: "Ms. "+x.name}):			    		
												Object.defineProperty(x,"name",{value: "Shmlue. "+x.name});
processedString=originalArray.filter(hasGenderAndName).map(addNamePrefix).map(JSON.stringify).join("\n----\n");
console.log(processedString);
