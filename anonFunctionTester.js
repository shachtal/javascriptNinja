function asyncCall({resolveOrReject,currentTimeStamp}){
	console.log(currentTimeStamp+" was invoked with "+resolveOrReject);
	return new Promise(function(resolve,reject){
		switch(resolveOrReject){
			case "resolve":
				let nextCurrentTimeStamp=Date.now()
				setTimeout(resolve,1000,{resolveOrReject:(Math.random()>0.25)?"resolve":"reject",currentTimeStamp:nextCurrentTimeStamp});
			break;
			case "reject":	
				setTimeout(reject,1000,new Error(currentTimeStamp+" was rejected"));
			break
			default:
		}
	})
}

function logger(err){
	console.log("reached end of life!!!!");
	console.log(err);
}

asyncCall({resolveOrReject:"resolve",currentTimeStamp:"firstPromiseInChain"})
	.then(asyncCall)
	.then(asyncCall)
	.then(asyncCall)
	.then(asyncCall)
	.then(asyncCall).catch(logger);

//setInterval(console.log,10000,"pulseMaker")
