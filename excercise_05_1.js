function stopperMaker(){
	let state={}
	return {
		start:function(measurementName){
			state[measurementName]=Date.now();
		},
		end:function(measurementName){
			if(state[measurementName]){
				let answer=Date.now()-state[measurementName];
				delete state[measurementName];
				return answer
			}else{
				return `no measurement named '${measurementName}' exists`;
			}
		}
	}
}

var myStopper=stopperMaker();
var myStopper_2=stopperMaker();
myStopper.start("first"); 
myStopper.start("second");
console.log( myStopper_2.end("second"));
console.log(myStopper.end("second"));
console.log(myStopper.end("first"));
