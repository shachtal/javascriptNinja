console.log(`01 - ----------printedFromOutsideOfFunctionBeforeDeclerations--------`);
console.log(`global1::${global1}`);  			//scoped with undefined, (hoisted but not assigned yet)
//console.log(`global2::${global2}`);			//unscoped becuase its an unhoisted global
//console.log(`global3::${global3}`);			//unscoped becuase its an unhoisted global
//console.log(`local::${local}`);				//unscoped because its a local
var global1 = "global with var outside of any function"
global2 = "global with no var outside of any function"

function scopingFunction(){
	console.log(`04 - ----------printedFromInsideFunctionBeforeDeclerations--------`);
	console.log(`global1::${global1}`);		//scoped with value (global that was assigned)
	console.log(`global2::${global2}`);		//scoped with value (global that was assigned)
	//console.log(`global3::${global3}`);	//unscoped because global has no var
	console.log(`global4::${global4}`);		//scoped with value (global that was assigned before function call)
	console.log(`global5::${global5}`);		//scoped with undefined (global that was assigned after function call)
	console.log(`local::${local}`);			//scoped with undefined (has var, but the local was not assigned yet)
	global3="global with no var inside a function"
	var local = "local with var inside a function"
	console.log(`05 - ----------printedFromInsideFunctionAfterDeclerations--------`);
	console.log(`$global1::${global1}`);	//scoped with value (var global assigned before function call)
	console.log(`$global2::${global2}`);	//scoped with value (non var global, assigned before function call)
	console.log(`$global3::${global3}`);	//scoped with value (non var global defined inside function, assigned before function call)
	console.log(`$local::${local}`);		//scoped with value (local inside function, already assinged)
}
console.log(`02 - ----------printedFromOutsideOfFunctionAfterDeclerationsBeforeCall--------`);
console.log(`global1::${global1}`);			//scoped with value (assigned global)
console.log(`global2::${global2}`);			//scoped with value (assigned global)
//console.log(`global3::${global3}`);		//unscoped because function was not called yet
//console.log(`local::${local}`);			//unscoped because local variable is not scoped outside of function

global4 = "global with no var outside of any function before function call"
scopingFunction();
var global5 = "global with var outside of any function after function call"
console.log(`03 - ----------printedFromOutsideOfFunctionAfterDeclerationsAfterCall--------`);
console.log(`global3::${global3}`);			//scoped because global variable was assigned in function call

